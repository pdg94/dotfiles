# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

# Terminal goodies
pfetch
eval "$(starship init bash)"

# sway env
export XCURSOR_THEME=Breeze_Obsidian

# Aliases

# Configuration files
alias swayc='vim .config/sway/config'
alias bashrc='vim .bashrc'

# Package manager
alias install='sudo xbps-install -S'
alias update='sudo xbps-install -Su'
alias search='xbps-query -Rs'
alias flatupdate='flatpak update'

# Runit
alias services='sudo sv status /var/service/*'
