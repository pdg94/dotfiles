# Dotfiles

Some of the configuration files i use in my GNU/Linux and BSD systems, including tiling window managers / Wayland compositors, panels, notifications daemon etc.

## "Installation"

To replicate my setup, you can manually copy the dotfiles to the correct folders, or you can use the GNU Stow software. Follow these instructions:
- clone this project into your home directory;
- cd into the dotfiles folder;
- give the command "stow < dir_1 > < dir_2 > ... < dir_k >", where dir_i is the directory that contains the dotfile you desire (for example the sway config, the i3 config, etc).

